package notifyfacade

import (
	"gitlab.com/beearn/chanpubsub"
	"gitlab.com/beearn/entity"
	"gitlab.com/beearn/notify"
	"log"
)

type NotifyFacade struct {
	ps chanpubsub.PublishSubscriber
	*notify.Notify
	tgHandler *TelegramHandler
	topics    []string
	title     string
}

type Opt func(*NotifyFacade)

func NewNotifyFacade(opts ...Opt) *NotifyFacade {
	s := &NotifyFacade{}

	for _, opt := range opts {
		opt(s)
	}
	if s.ps == nil || s.Notify == nil || len(s.topics) == 0 || s.tgHandler == nil {
		panic("NotifyFacade: missing required params: pub sub, notify, topics, tghandler")
	}

	return s
}

func WithPubSub(ps chanpubsub.PublishSubscriber) Opt {
	return func(s *NotifyFacade) {
		s.ps = ps
	}
}

func WithTelegramHandler(tgHandler *TelegramHandler) Opt {
	return func(s *NotifyFacade) {
		s.tgHandler = tgHandler
	}
}

func WithNotify(notify *notify.Notify) Opt {
	return func(s *NotifyFacade) {
		s.Notify = notify
	}
}

func WithTopics(topics []string) Opt {
	return func(s *NotifyFacade) {
		s.topics = topics
	}
}

func WithTitle(title string) Opt {
	return func(s *NotifyFacade) {
		s.title = title
	}
}

// Serve starts the subscription
func (s *NotifyFacade) Serve() {
	s.Send(entity.MessageNotify{
		Text: s.title,
	})
	go s.tgHandler.Serve()
	for _, topic := range s.topics {
		stream := s.ps.Subscribe(topic)
		go func() {
			for {
				select {
				case msg := <-stream:
					err := s.Send(msg.Data.(entity.MessageNotify))
					if err != nil {
						log.Println(err)
					}
				}
			}
		}()
	}
}
