module gitlab.com/beearn/notifyfacade

go 1.19

require (
	gitlab.com/beearn/chanpubsub v1.0.2
	gitlab.com/beearn/entity v1.0.0
	gitlab.com/beearn/notify v1.0.0
	gopkg.in/telebot.v3 v3.1.3
)
