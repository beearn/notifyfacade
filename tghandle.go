package notifyfacade

import (
	"gitlab.com/beearn/chanpubsub"
	"gitlab.com/beearn/entity"
	tele "gopkg.in/telebot.v3"
)

type TelegramHandler struct {
	tg                 *tele.Bot
	DefaultRecipientID int64
	PubSub             chanpubsub.PublishSubscriber
	init               bool
}

type Opts func(*TelegramHandler)

func NewTelegramHandler(opts ...Opts) *TelegramHandler {
	n := &TelegramHandler{}

	for _, opt := range opts {
		opt(n)
	}

	if n.tg == nil {
		panic("Telegram bot is not initialized")
	}

	if n.PubSub == nil {
		panic("pubsub is required")
	}

	return n
}

func HandleWithPubSub(pubSub chanpubsub.PublishSubscriber) Opts {
	return func(n *TelegramHandler) {
		n.PubSub = pubSub
	}
}

func WithTelegram(tg *tele.Bot) Opts {
	return func(n *TelegramHandler) {
		n.tg = tg
	}
}

func WithDefaultRecipientID(id int64) Opts {
	return func(n *TelegramHandler) {
		n.DefaultRecipientID = id
	}
}

func (t *TelegramHandler) Send(msg entity.MessageNotify) error {
	if t.tg == nil {
		return nil
	}
	if msg.RecipientID == 0 {
		msg.RecipientID = t.DefaultRecipientID
	}
	var tgOpts []interface{}
	recipient := &tele.User{ID: msg.RecipientID}
	if msg.Format == "md" {
		tgOpts = append(tgOpts, tele.ModeMarkdown)
	}
	_, err := t.tg.Send(recipient, msg.Text, tgOpts...)

	return err
}

func (t *TelegramHandler) RegisterOnText(hndls ...func(handler *TelegramHandler) tele.HandlerFunc) {
	for _, hndl := range hndls {
		t.tg.Handle(tele.OnText, hndl(t))
	}
	t.init = true
}

func (t *TelegramHandler) Serve() {
	if !t.init {
		panic("TelegramHandler: not initialized")
	}
	go t.tg.Start()
}
